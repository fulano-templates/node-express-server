// imports
const express = require('express');
const cors = require('cors');
const router = require('./router');

//! para facilitar o entendimento dos comentarios instale a extenção better-comments no seu vscode

const app = express(); //? inicia a variavel app com o express

app.use(cors()); //? o cors que permite que varios dominios acessem a aplicação
app.use(express.json()); //? definindo que as requisições do server serão em json 
app.use(router); //? passamos as rotas do banco (criadas no router.js)

module.exports = app; //? exporta o app para ser usado no server.js